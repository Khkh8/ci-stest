const chai = require("chai")
const expext = chai.expect
const request = require("request")
const app = require("../src/server")
const port = 3000

let server

/** async request */
const arequest = async (value) => new Promise((resolve, reject) => {
    request(value, (error, response) => {
        if (error) reject(error)
        else resolve(response)
    })
})

describe("Test REST API", () => {
    beforeEach("Start server", async () => {
        server = app.listen(port)
    })
    describe("Test functionality", () => {
        it("GET /add?a=3&b=4", async () => {
            const options = {
                method: 'GET',
                url: 'http://localhost:3000/add',
                qs: { a: '3', b: '4' },
            }
            await arequest(options).then((res) => {
                console.log({ message: res.body })
                expext(res.body).to.equal("7")
            }).catch((res) => {
                console.log({res})
                expext(true).to.equal(false, "Add function failed")
            })
        })
    })

    // Check if any test fails and close the server if so
    afterEach(() => {
        server.close()
    })
})